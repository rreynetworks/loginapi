const express = require('express');
const router = express.Router();
const User = require('../models/User');
const jwt = require('jsonwebtoken');

// Ruta de registro de usuario
router.post('/register', async (req, res) => {
  const { username, password } = req.body;

  try {
    // Crear un nuevo usuario
    const newUser = new User({ username, password });
    await newUser.save();
    res.status(201).json({ message: 'Usuario registrado exitosamente' });
  } catch (error) {
    res.status(500).json({ message: 'Error al registrar el usuario', error });
  }
});

// Ruta de inicio de sesión
router.post('/login', async (req, res) => {
  const { username, password } = req.body;

  try {
    // Buscar al usuario por nombre de usuario
    const user = await User.findOne({ username });
    if (!user) {
      return res.status(401).json({ message: 'Autenticación fallida: Usuario no encontrado' });
    }

    // Comparar la contraseña
    const isMatch = await user.comparePassword(password);
    if (!isMatch) {
      return res.status(401).json({ message: 'Autenticación fallida: Contraseña incorrecta' });
    }

    // Crear y enviar el token JWT
    const token = jwt.sign({ id: user._id }, 'secreto', { expiresIn: '1h' });
    res.status(200).json({ message: 'Autenticación satisfactoria', token });
  } catch (error) {
    res.status(500).json({ message: 'Error en la autenticación', error });
  }
});

module.exports = router;